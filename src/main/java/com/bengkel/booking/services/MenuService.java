package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<BookingOrder> listAllBookingOrders = new ArrayList<>();
	private static Customer selectedCustomer = null;
	public static void run() {
		String[] listStartMenu = {"Login", "Exit"};
		boolean isLooping = true;
		int startChoice = 0;
		do {
			PrintService.printMenu(listStartMenu, "Booking Bengkel");
			startChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listStartMenu.length-1, 0);
			switch (startChoice) {
				case 1:
					login();
					if (selectedCustomer != null) {
						mainMenu();
					} else isLooping = false;
					break;
			
				default:
					isLooping = false;
					break;
			}
		} while (isLooping);
		
	}
	
	public static void login() {
		selectedCustomer = BengkelService.loginValidation(listAllCustomers);
	}
	
	public static void mainMenu() {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			// System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				//panggil fitur Informasi Customer
				PrintService.printCustomer(selectedCustomer);
				break;
			case 2:
				//panggil fitur Booking Bengkel
				listAllBookingOrders = BengkelService.setNewBookingOrder(selectedCustomer, listAllItemService, listAllBookingOrders);
				break;
			case 3:
				//panggil fitur Top Up Saldo Coin
				BengkelService.topUpSaldoMemberCustomer(selectedCustomer);
				break;
			case 4:
				//panggil fitur Informasi Booking Order
				PrintService.printBookingOrder(listAllBookingOrders);
				break;
			default:
				System.out.println("Logout");
				selectedCustomer = null;
				isLooping = false;
				break;
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
