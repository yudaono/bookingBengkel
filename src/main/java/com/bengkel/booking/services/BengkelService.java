package com.bengkel.booking.services;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.border.StrokeBorder;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class BengkelService {
	
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	private static Scanner input = new Scanner(System.in);
	private static int bookingIdNum = 1;
	
	//Login
	public static Customer loginValidation(List<Customer> listAllCustomers){
		Customer selectedCustomer = null;
		int trialCount = 0;
		
		
		do {
			System.out.printf("Masukan Customer Id : ");
			String inputUsername = input.nextLine();
			System.out.printf("Masukan Password : ");
			String inputPassword = input.nextLine();
			selectedCustomer = listAllCustomers.stream()
										.filter(customer -> customer.getCustomerId().equals(inputUsername) && customer.getPassword().equals(inputPassword))
										.findFirst().orElse(null);

			if (selectedCustomer == null) {
				System.out.println("Customer Id atau password Salah!");
			}
			trialCount++;
		} while (selectedCustomer==null && trialCount < 3);

		return selectedCustomer;
	}
	
	//Info Customer
	public static String getMembershipStatus(Customer customer){
		String result = "Non Member";
		if (customer instanceof MemberCustomer) {
			result = "Member";
		}

		return result;
	}
	
	//Booking atau Reservation
	public static List<BookingOrder> setNewBookingOrder(Customer customer, List<ItemService> listAllItemServices, List<BookingOrder> listAllBookingOrders){
		List<ItemService> listSelectedService = new ArrayList<>();
		Vehicle selectedVehicle = null;
		double price = 0;
		boolean isLooping = true;
		int selectCount = 0;
		
		System.out.println("Booking Bengkel\n");
		System.out.println("Masukan Vehicle Id :");
		do {
			String inputVehicleId = input.nextLine();
			if (inputVehicleId.equals("0")) {
				return listAllBookingOrders;
			}

			selectedVehicle = customer.getVehicles().stream()
								.filter(vehicle -> vehicle.getVehiclesId().equals(inputVehicleId))
								.findFirst().orElse(null);

			if (selectedVehicle == null) {
				System.out.println("Vehicle Id Tidak ditemukan");
			}
		} while (selectedVehicle == null);

		System.out.println("List Service :");
		PrintService.printService(listAllItemServices, selectedVehicle.getVehicleType());

		do {
			System.out.println("Sihlakan masukan Service Id :");
			ItemService selectedItemService = null;

			do {
				String inputServiceId = input.nextLine();
				selectedItemService = listAllItemServices.stream()
										.filter(itemService -> itemService.getServiceId().equals(inputServiceId))
										.findFirst().orElse(null);
				if (selectedItemService == null) {
					System.out.println("Service tidak ditemukan, masukan kembali :");
				} else if (listSelectedService.contains(selectedItemService)){
					selectedItemService = null;
					System.out.println("Data sudah dimasukan, masukan kembali :");
				}
			} while (selectedItemService == null);

			listSelectedService.add(selectedItemService);

			if (getMembershipStatus(customer).equals("Member") && selectCount < 1) {
				String inputLoopingBooking = Validation.validasiInput("Apakah anda ingin menambahkan Service Lainnya? (Y/T) : ", "Input Salah!", "^[Y|T]*$");
				if (inputLoopingBooking.equals("Y")) {
					isLooping = false;
				}
				selectCount++;
			} else isLooping = true;
		} while (!isLooping);

		for (ItemService itemService : listSelectedService) {
			price = price + itemService.getPrice();
		}

		double discountPrice = price;
		String inputPaymentMethod = "Cash";
		if (getMembershipStatus(customer).equals("Member")) {
			inputPaymentMethod = Validation.validasiInput("Pilih Metode Pembayaran (Saldo Coin / Cash) : ", "Input tidak tersedia, Masukan kembali", "^[Saldo Coin|Cash]*$");
			if (inputPaymentMethod.equals("Saldo Coin")) {
				discountPrice = price - (price * 0.1);
				MemberCustomer memberCustomer = (MemberCustomer) customer;
				memberCustomer.setSaldoCoin(memberCustomer.getSaldoCoin() - discountPrice);
			}
		}

		String bookingId = String.format("Book-%s-%03d", customer.getCustomerId(), bookingIdNum);
		bookingIdNum++;

		listAllBookingOrders.add(new BookingOrder(bookingId, customer, listSelectedService, inputPaymentMethod, price, discountPrice));
		System.out.println("Booking Berhasil");
		System.out.printf("Total Harga Service : %d\n", (int) price);
		System.out.printf("Total Pembayaran : %d\n", (int) discountPrice);

		return listAllBookingOrders;
	}
	
	//Top Up Saldo Coin Untuk Member Customer
	public static Customer topUpSaldoMemberCustomer(Customer selectedCustomer){
		if (getMembershipStatus(selectedCustomer).equals("Member")) {
			MemberCustomer customer = (MemberCustomer) selectedCustomer;
			System.out.printf("Masukan jumlah Saldo yang akan di Top Up :");
			double newSaldo = input.nextDouble();
			input.nextLine();
			customer.setSaldoCoin(customer.getSaldoCoin() + newSaldo);
			selectedCustomer = customer;
		} else System.out.println("Fitur ini hanya untuk Member Saja");

		return selectedCustomer;
	}
	
	//Logout
	
}
