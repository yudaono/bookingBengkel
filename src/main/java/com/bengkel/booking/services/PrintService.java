package com.bengkel.booking.services;

import java.security.Provider.Service;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}

	public static void printService(List<ItemService> listAllItemService, String vehicleType) {
		String formatTable = "| %-2s | %-15s | %-20s | %-18s | %15s |%n";
		String line = "+----+-----------------+----------------------+--------------------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
	    System.out.format(line);
	    int number = 1;
	    for (ItemService service : listAllItemService) {
			if (vehicleType.equals(service.getVehicleType())) {
				System.out.format(formatTable, number, service.getServiceId(), service.getServiceName(), service.getVehicleType(), service.getPrice());
				number++;
			}
	    }
	    System.out.printf(line);
		System.out.format("| %-2s | %-77s |%n", "0", "Kembali Ke Home Menu");
	    System.out.printf(line);
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.
	public static void printCustomer(Customer customer){
		System.out.println("		Customer Profile		");
		String formatData = "%-20s : %-10s %n";
		System.out.format(formatData, "Customer Id", customer.getCustomerId());
		System.out.format(formatData, "Nama", customer.getName());
		System.out.format(formatData, "Nama", BengkelService.getMembershipStatus(customer));
		System.out.format(formatData, "Nama", customer.getAddress());
		if (BengkelService.getMembershipStatus(customer).equals("Member")) {
			MemberCustomer newCustomer = (MemberCustomer) customer;
			System.out.format(formatData, "Saldo Koin", newCustomer.getSaldoCoin());
		}

		System.out.println("List Kendaraan :");
		printVechicle(customer.getVehicles());
	}

	public static String printListVehicleServices(List<ItemService> listItemServices){
		String result = "";
		for (int index = 0; index < listItemServices.size(); index++) {
			if (index > 0) {
				result = result + ",";
			}
			result = result + listItemServices.get(index).getServiceName();
		}
		return result;
	}

	public static void printBookingOrder(List<BookingOrder> listAllBookingOrders){
		System.out.println("		Booking Order		");
		String formatTable = "| %-2s | %-30s | %-20s | %-30s | %-15s | %-15s | %-30s |%n";
		String line = "+----+--------------------------------+----------------------+--------------------------------+-----------------+-----------------+--------------------------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment", "List Service");
	    System.out.format(line);
	    int number = 1;
		for (BookingOrder bookingOrder : listAllBookingOrders) {
			System.out.format(formatTable, number, bookingOrder.getBookingId(), bookingOrder.getCustomer().getName(), bookingOrder.getPaymentMethod(), bookingOrder.getTotalServicePrice(), bookingOrder.getTotalPayment(), printListVehicleServices(bookingOrder.getServices()));
			number++;
		}
		System.out.format(line);
	}
	
}
